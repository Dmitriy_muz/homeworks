    document.ondragstart = noselect; 
    document.onselectstart = noselect;  
    function noselect() {return false;}  

$( function() {
    $( "#tabs" ).tabs();
  } );

  let auxiliaryClasses = [".auxiliary-class10", ".auxiliary-class11", ".auxiliary-class12", ".auxiliary-class13", ".auxiliary-class14"]
  let tabsauxiliaryClasses = [".tabs-auxiliary-Class1", ".tabs-auxiliary-Class2", ".tabs-auxiliary-Class3", ".tabs-auxiliary-Class4", ".tabs-auxiliary-Class5",]  
  $(auxiliaryClasses[0]).click(
      function()
      {
       $(".tabs-page3").css("border-top", "none")
       $(".tabs-link2").css("color", "#717171")
       $(auxiliaryClasses[0]).css("border-top", "3px solid #18cfab")
       $(tabsauxiliaryClasses[0]).css("color", "#18cfab")
     })

     $(auxiliaryClasses[1]).click(
      function()
      {
       $(".tabs-page3").css("border-top", "none")
       $(".tabs-link2").css("color", "#717171")
       $(auxiliaryClasses[1]).css("border-top", "3px solid #18cfab")
       $(tabsauxiliaryClasses[1]).css("color", "#18cfab")
     })

     $(auxiliaryClasses[2]).click(
      function()
      {
       $(".tabs-page3").css("border-top", "none")
       $(".tabs-link2").css("color", "#717171")
       $(auxiliaryClasses[2]).css("border-top", "3px solid #18cfab")
       $(tabsauxiliaryClasses[2]).css("color", "#18cfab")
     })

     $(auxiliaryClasses[3]).click(
      function()
      {
       $(".tabs-page3").css("border-top", "none")
       $(".tabs-link2").css("color", "#717171")
       $(auxiliaryClasses[3]).css("border-top", "3px solid #18cfab")
       $(tabsauxiliaryClasses[3]).css("color", "#18cfab")
     })

     $(auxiliaryClasses[4]).click(
      function()
      {
       $(".tabs-page3").css("border-top", "none")
       $(".tabs-link2").css("color", "#717171")
       $(auxiliaryClasses[4]).css("border-top", "3px solid #18cfab")
       $(tabsauxiliaryClasses[4]).css("color", "#18cfab")
     })

  let loadmorebuttonstatus = 0

     $(".load-more-button").click(
      function()
      {
        $(".load-more-button").css("display", "none")
        $(".li-photos2").css("display", "inline-block")
        $(".fourth-page-headline").css("margin-top", "100px")
         loadmorebuttonstatus = 1
      }
    )

    $(".auxiliary-class11").click(
      function()
      {
        $(".tab2-pg3").css("display", "block")
        $(".tab1-pg3").css("display", "none")
        $(".tab3-pg3").css("display", "none")
        $(".tab4-pg3").css("display", "none")
        $(".tab5-pg3").css("display", "none")
        $(".fourth-page-headline").css("margin-top", "100px")
      }
    )
    $(".auxiliary-class10").click(
      function()
      {
        $(".tab1-pg3").css("display", "block")
        $(".tab2-pg3").css("display", "none")
        $(".tab3-pg3").css("display", "none")
        $(".tab4-pg3").css("display", "none")
        $(".tab5-pg3").css("display", "none")
        if(loadmorebuttonstatus === 1)
        {
          $(".fourth-page-headline").css("margin-top", "100px")
        }
        else
        {
          $(".fourth-page-headline").css("margin-top", "-300px")
        }
      }
    )
    $(".auxiliary-class12").click(
      function()
      {
        $(".tab3-pg3").css("display", "block")
        $(".tab2-pg3").css("display", "none")
        $(".tab1-pg3").css("display", "none")
        $(".tab4-pg3").css("display", "none")
        $(".tab5-pg3").css("display", "none")
        $(".fourth-page-headline").css("margin-top", "100px")
      }
    )
    $(".auxiliary-class13").click(
      function()
      {
        $(".tab4-pg3").css("display", "block")
        $(".tab2-pg3").css("display", "none")
        $(".tab1-pg3").css("display", "none")
        $(".tab3-pg3").css("display", "none")
        $(".tab5-pg3").css("display", "none")
        $(".fourth-page-headline").css("margin-top", "100px")
      }
    )
    $(".auxiliary-class14").click(
      function()
      {
        $(".tab5-pg3").css("display", "block")
        $(".tab2-pg3").css("display", "none")
        $(".tab1-pg3").css("display", "none")
        $(".tab3-pg3").css("display", "none")
        $(".tab4-pg3").css("display", "none")
        $(".fourth-page-headline").css("margin-top", "100px")
      }
    )

    $(".bn-li1").click(
      function()
      {
        $(".news-data-div").css("background-color", "#203e38")
        $(".post-headline").css("color", "#717171")
        $(".data-div1").css("background-color", "#18cfab")
        $(".headline-click1").css("color", "#18cfab")
      }
    )


    $(".bn-li2").click(
      function()
      {
        $(".news-data-div").css("background-color", "#203e38")
        $(".post-headline").css("color", "#717171")
        $(".data-div2").css("background-color", "#18cfab")
        $(".headline-click2").css("color", "#18cfab")
      }
    )
    
    
    $(".bn-li3").click(
      function()
      {
        $(".news-data-div").css("background-color", "#203e38")
        $(".post-headline").css("color", "#717171")
        $(".data-div3").css("background-color", "#18cfab")
        $(".headline-click3").css("color", "#18cfab")
      }
    )
    
    
    $(".bn-li4").click(
      function()
      {
        $(".news-data-div").css("background-color", "#203e38")
        $(".post-headline").css("color", "#717171")
        $(".data-div4").css("background-color", "#18cfab")
        $(".headline-click4").css("color", "#18cfab")
      }
    )
    
    
    $(".bn-li5").click(
      function()
      {
        $(".news-data-div").css("background-color", "#203e38")
        $(".post-headline").css("color", "#717171")
        $(".data-div5").css("background-color", "#18cfab")
        $(".headline-click5").css("color", "#18cfab")
      }
    )
    
    
    $(".bn-li6").click(
      function()
      {
        $(".news-data-div").css("background-color", "#203e38")
        $(".post-headline").css("color", "#717171")
        $(".data-div6").css("background-color", "#18cfab")
        $(".headline-click6").css("color", "#18cfab")
      }
    )
    
    
    $(".bn-li7").click(
      function()
      {
        $(".news-data-div").css("background-color", "#203e38")
        $(".post-headline").css("color", "#717171")
        $(".data-div7").css("background-color", "#18cfab")
        $(".headline-click7").css("color", "#18cfab")
      }
    )
    
    
    $(".bn-li8").click(
      function()
      {
        $(".news-data-div").css("background-color", "#203e38")
        $(".post-headline").css("color", "#717171")
        $(".data-div8").css("background-color", "#18cfab")
        $(".headline-click8").css("color", "#18cfab")
      }
    )




    $(".reviewer1").mouseenter(
      function ()
      {
        $(".rev-imgblock1").css("margin-top", "-58px") 
        $(".rev-imgblock1").css("transition", "margin-top 0.3s") 
      }
    )
    $(".reviewer1").mouseleave(
      function ()
      {
        if(reviewer1status === 0)
        {
        $(".rev-imgblock1").css("margin-top", "-45px") 
        $(".rev-imgblock1").css("transition", "margin-top 0.3s") 
        }
      }
    )


    $(".reviewer2").mouseenter(
      function ()
      {
        $(".rev-imgblock2").css("margin-top", "-58px") 
        $(".rev-imgblock2").css("transition", "margin-top 0.3s") 
      }
    )
    $(".reviewer2").mouseleave(
      function ()
      {
        if(reviewer2status === 0)
        {
        $(".rev-imgblock2").css("margin-top", "-45px") 
        $(".rev-imgblock2").css("transition", "margin-top 0.3s") 
        }
      }
    )


    $(".reviewer3").mouseenter(
      function ()
      {
        $(".rev-imgblock3").css("margin-top", "-58px") 
        $(".rev-imgblock3").css("transition", "margin-top 0.3s") 
      }
    )
    $(".reviewer3").mouseleave(
      function ()
      {
        if(reviewer3status === 0)
        {
        $(".rev-imgblock3").css("margin-top", "-45px") 
        $(".rev-imgblock3").css("transition", "margin-top 0.3s") 
        }
      }
    )


    $(".reviewer4").mouseenter(
      function ()
      {
        $(".rev-imgblock4").css("margin-top", "-58px") 
        $(".rev-imgblock4").css("transition", "margin-top 0.3s") 
      }
    )
    $(".reviewer4").mouseleave(
      function ()
      {
        if(reviewer4status === 0)
        {
        $(".rev-imgblock4").css("margin-top", "-45px") 
        $(".rev-imgblock4").css("transition", "margin-top 0.3s") 
        }
      }
    )


      let currentrev = 3
      // let reviewers = ["reviewer1", "reviewer2", "reviewer3", "reviewer4",]
            
            
            let reviewer1status = 0
              $(".reviewer1").click(
                function reviewer1()
                {
                  currentrev = 1
                  console.log("currrev = 1")
                  reviewer1status = 1
                  reviewer3status = 0
                  reviewer2status = 0
                  reviewer4status = 0
                  $(".rev-imgblock1").css("border", "1.1px solid #18cfab")
                  $(".rev-imgblock2").css("border", "none")
                  $(".rev-imgblock3").css("border", "none")
                  $(".rev-imgblock4").css("border", "none")

                  $(".rev-imgblock2").css("margin-top", "-45px")
                  $(".rev-imgblock3").css("margin-top", "-45px")
                  $(".rev-imgblock4").css("margin-top", "-45px")
                  
                  $(".reviewer-name").html("Ham customer")
                  $(".reviewer-specialization").html("Customer")
                  $(".reviewer-photo").attr("src", "StepProjectHam/additional files/Image Here2.png")
              }
              )
            
          

            
              let reviewer2status = 0
              $(".reviewer2").click(
                function reviewer2()
                {
                  currentrev  = 2
                  console.log("currev=2")
                  reviewer2status = 1
                  reviewer3status = 0
                  reviewer4status = 0
                  reviewer1status = 0
                  $(".rev-imgblock2").css("border", "1.1px solid #18cfab")
                  $(".rev-imgblock1").css("border", "none")
                  $(".rev-imgblock3").css("border", "none")
                  $(".rev-imgblock4").css("border", "none")
                  
                  $(".rev-imgblock1").css("margin-top", "-45px")
                  $(".rev-imgblock3").css("margin-top", "-45px")
                  $(".rev-imgblock4").css("margin-top", "-45px")

                  $(".reviewer-name").html("Ramzan Noni")
                  $(".reviewer-specialization").html("UI Designer")
                  $(".reviewer-photo").attr("src", "StepProjectHam/additional files/Image Here3.png")
              }
              )
            


             
              let reviewer3status = 1
              $(".reviewer3").click(
                function reviewer3()
                {
                  currentrev = 3
                  console.log("currev=3")
                  reviewer3status = 1
                  reviewer4status = 0
                  reviewer2status = 0
                  reviewer1status = 0
                  $(".rev-imgblock3").css("border", "1.1px solid #18cfab")
                  $(".rev-imgblock2").css("border", "none")
                  $(".rev-imgblock1").css("border", "none")
                  $(".rev-imgblock4").css("border", "none")
                  
                  $(".rev-imgblock2").css("margin-top", "-45px")
                  $(".rev-imgblock1").css("margin-top", "-45px")
                  $(".rev-imgblock4").css("margin-top", "-45px")
                  
                  $(".reviewer-name").html("Hasan Ali")
                  $(".reviewer-specialization").html("UX Designer")
                  $(".reviewer-photo").attr("src", "StepProjectHam/additional files/Image Here.png")
              }
              )
            


             
            let reviewer4status = 0
              $(".reviewer4").click(
                function reviewer4()
                {
                  currentrev = 4
                  console.log("currev=4")
                  reviewer4status = 1
                  reviewer3status = 0
                  reviewer2status = 0
                  reviewer1status = 0
                  $(".rev-imgblock4").css("border", "1.1px solid #18cfab")
                  $(".rev-imgblock4").css("margin-top", "-58px")
                  $(".rev-imgblock2").css("border", "none")
                  $(".rev-imgblock3").css("border", "none")
                  $(".rev-imgblock1").css("border", "none")
                  
                  $(".rev-imgblock2").css("margin-top", "-45px")
                  $(".rev-imgblock3").css("margin-top", "-45px")
                  $(".rev-imgblock1").css("margin-top", "-45px")
                  
                  $(".reviewer-name").html("Ham Member")
                  $(".reviewer-specialization").html("UI Designer")
                  $(".reviewer-photo").attr("src", "StepProjectHam/additional files/Image Here4.png")
              }
              )

            $(".arrow-back-block").click(
              function()
              {
                if(currentrev === 2)
                {
                  // reviewer1()
                  currentrev = 1
                  console.log("currrev = 1")
                  reviewer1status = 1
                  reviewer3status = 0
                  reviewer2status = 0
                  reviewer4status = 0
                  $(".rev-imgblock1").css("border", "1.1px solid #18cfab")
                  $(".rev-imgblock2").css("border", "none")
                  $(".rev-imgblock3").css("border", "none")
                  $(".rev-imgblock4").css("border", "none")

                  $(".rev-imgblock1").css("margin-top", "-58px")
                  $(".rev-imgblock1").css("transition", "margin-top 0.3s")
                  $(".rev-imgblock2").css("margin-top", "-45px")
                  $(".rev-imgblock3").css("margin-top", "-45px")
                  $(".rev-imgblock4").css("margin-top", "-45px")
                  
                  $(".reviewer-name").html("Ham customer")
                  $(".reviewer-specialization").html("Customer")
                  $(".reviewer-photo").attr("src", "StepProjectHam/additional files/Image Here2.png") 
                }
                if(currentrev === 3)
                {
                  // reviewer2()
                  currentrev  = 2
                  console.log("currev=2")
                  reviewer2status = 1
                  reviewer3status = 0
                  reviewer4status = 0
                  reviewer1status = 0
                  $(".rev-imgblock2").css("border", "1.1px solid #18cfab")
                  $(".rev-imgblock1").css("border", "none")
                  $(".rev-imgblock3").css("border", "none")
                  $(".rev-imgblock4").css("border", "none")
                  
                  $(".rev-imgblock2").css("margin-top", "-58px") 
                  $(".rev-imgblock2").css("transition", "margin-top 0.3s") 
                  $(".rev-imgblock1").css("margin-top", "-45px")
                  $(".rev-imgblock3").css("margin-top", "-45px")
                  $(".rev-imgblock4").css("margin-top", "-45px")

                  $(".reviewer-name").html("Ramzan Noni")
                  $(".reviewer-specialization").html("UI Designer")
                  $(".reviewer-photo").attr("src", "StepProjectHam/additional files/Image Here3.png")
                }
                if(currentrev === 4)
                {
                  currentrev = 3
                  console.log("currev=3")
                  reviewer3status = 1
                  reviewer4status = 0
                  reviewer2status = 0
                  reviewer1status = 0
                  $(".rev-imgblock3").css("border", "1.1px solid #18cfab")
                  $(".rev-imgblock2").css("border", "none")
                  $(".rev-imgblock1").css("border", "none")
                  $(".rev-imgblock4").css("border", "none")
                  
                  $(".rev-imgblock3").css("margin-top", "-58px") 
                  $(".rev-imgblock3").css("transition", "margin-top 0.3s") 
                  $(".rev-imgblock2").css("margin-top", "-45px")
                  $(".rev-imgblock1").css("margin-top", "-45px")
                  $(".rev-imgblock4").css("margin-top", "-45px")
                  
                  $(".reviewer-name").html("Hasan Ali")
                  $(".reviewer-specialization").html("UX Designer")
                  $(".reviewer-photo").attr("src", "StepProjectHam/additional files/Image Here.png")
                }
              }
            )



            $(".arrow-forward-block").click(
              function()
              {
                if(currentrev === 3)
                {
                  currentrev = 4
                  console.log("currev=4")
                  reviewer4status = 1
                  reviewer3status = 0
                  reviewer2status = 0
                  reviewer1status = 0
                  $(".rev-imgblock4").css("border", "1.1px solid #18cfab")
                  $(".rev-imgblock4").css("margin-top", "-58px")
                  $(".rev-imgblock2").css("border", "none")
                  $(".rev-imgblock3").css("border", "none")
                  $(".rev-imgblock1").css("border", "none")
                  
                  $(".rev-imgblock4").css("margin-top", "-58px") 
                  $(".rev-imgblock4").css("transition", "margin-top 0.3s") 
                  $(".rev-imgblock2").css("margin-top", "-45px")
                  $(".rev-imgblock3").css("margin-top", "-45px")
                  $(".rev-imgblock1").css("margin-top", "-45px")
                  
                  $(".reviewer-name").html("Ham Member")
                  $(".reviewer-specialization").html("UI Designer")
                  $(".reviewer-photo").attr("src", "StepProjectHam/additional files/Image Here4.png")
                }

                if(currentrev === 2)
                {
                  currentrev = 3
                  console.log("currev=3")
                  reviewer3status = 1
                  reviewer4status = 0
                  reviewer2status = 0
                  reviewer1status = 0
                  $(".rev-imgblock3").css("border", "1.1px solid #18cfab")
                  $(".rev-imgblock2").css("border", "none")
                  $(".rev-imgblock1").css("border", "none")
                  $(".rev-imgblock4").css("border", "none")
                  
                  $(".rev-imgblock3").css("margin-top", "-58px") 
                  $(".rev-imgblock3").css("transition", "margin-top 0.3s") 
                  $(".rev-imgblock2").css("margin-top", "-45px")
                  $(".rev-imgblock1").css("margin-top", "-45px")
                  $(".rev-imgblock4").css("margin-top", "-45px")
                  
                  $(".reviewer-name").html("Hasan Ali")
                  $(".reviewer-specialization").html("UX Designer")
                  $(".reviewer-photo").attr("src", "StepProjectHam/additional files/Image Here.png")
                }

                if(currentrev === 1)
                {
                  // reviewer2()
                  currentrev  = 2
                  console.log("currev=2")
                  reviewer2status = 1
                  reviewer3status = 0
                  reviewer4status = 0
                  reviewer1status = 0
                  $(".rev-imgblock2").css("border", "1.1px solid #18cfab")
                  $(".rev-imgblock1").css("border", "none")
                  $(".rev-imgblock3").css("border", "none")
                  $(".rev-imgblock4").css("border", "none")
                  
                  $(".rev-imgblock2").css("margin-top", "-58px") 
                  $(".rev-imgblock2").css("transition", "margin-top 0.3s") 
                  $(".rev-imgblock1").css("margin-top", "-45px")
                  $(".rev-imgblock3").css("margin-top", "-45px")
                  $(".rev-imgblock4").css("margin-top", "-45px")

                  $(".reviewer-name").html("Ramzan Noni")
                  $(".reviewer-specialization").html("UI Designer")
                  $(".reviewer-photo").attr("src", "StepProjectHam/additional files/Image Here3.png")
                }
              }
            )